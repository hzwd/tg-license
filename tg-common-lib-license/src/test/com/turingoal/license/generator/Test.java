package com.turingoal.license.generator;

import com.turingoal.license.TgLicense;
import net.lingala.zip4j.exception.ZipException;

import java.security.KeyPair;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Test
 */
public class Test {
    /**
     * s
     */
    public static void test() {
        TgLicense licenseInfo = new TgLicense();
        licenseInfo.setPackageName("com.turingoal.znxj");
        licenseInfo.setProduct("tg-znxj");
        licenseInfo.setLicenseType("开发版");
        licenseInfo.setIssuedDate(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        licenseInfo.setExpiryDate(LocalDate.now().plusDays(120).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        licenseInfo.setCustomer("图灵谷（北京）科技有限公司");
        licenseInfo.setCustomerType("企业");
        // licenseInfo.setVersionName("V1.0.0");
        // licenseInfo.setCustomerCodeNum("11111111111111111111111");
        // licenseInfo.setIpAddress("192.168.31.23");
        // licenseInfo.setMacAddress("E8-4E-06-35-93-84");
        KeyPair licenseKeyPair = TgLicenseKeyUtil.createKeyPair();
        if (licenseKeyPair != null) {
            System.out.println("PrivateKey: " + TgLicenseKeyUtil.getPrivateKeyStr(licenseKeyPair)); // 私钥保存在服务端
            System.out.println("PublicKey: " + TgLicenseKeyUtil.getPublicKeyStr(licenseKeyPair)); // 私钥保存在服务端
            licenseInfo.setPublicKey(TgLicenseKeyUtil.getPublicKeyStr(licenseKeyPair)); // 公钥给客户端
            System.out.println(licenseInfo);
            TgLicenseGenerator.createLicenseAndSaveFile(licenseInfo, licenseKeyPair, "D:/license.lic");
            try {
                TgLicenseGenerator.createLicenseAndSaveFileToZip(licenseInfo, licenseKeyPair, "D:/license.lic", "D:/test.zip");
            } catch (ZipException e) {
                throw new RuntimeException(e);
            }
            //    TgLicenseGenerator.createLicenseAndSaveFileToAliyunOss("oss-cn-zhangjiakou.aliyuncs.com", "", "", "turingoal", licenseInfo, "licenses/test/test1.bin");
            // 测试读取
            //   License l = TgLicenseUtil.readLicense("D:/license.lic");
//            if (l != null) {
//                System.out.println(l.get("licenseSign"));
//                System.out.println(l.getLicenseId() + " " + l.get("versionName"));
//                System.out.println(l.isOK(licenseInfo.getLicenseKeyPair().getPair().getPublic()));
//                TgLicenseChecker.getLicenseInfo();
//            }
        }
    }

    /**
     * main
     */
    public static void main(final String[] args) {
        test();
    }
}
