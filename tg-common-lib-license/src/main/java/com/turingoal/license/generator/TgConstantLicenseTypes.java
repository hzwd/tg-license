package com.turingoal.license.generator;

/**
 * 常量-》授权类型
 */
public interface TgConstantLicenseTypes {
    String LICENSE_TYPE_DEV = "开发版";
    String LICENSE_TYPE_FREE = "免费版";
    String LICENSE_TYPE_OFFICAL = "正式版";
}
