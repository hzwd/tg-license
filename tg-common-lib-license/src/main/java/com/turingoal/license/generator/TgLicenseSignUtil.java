package com.turingoal.license.generator;

import com.turingoal.license.TgLicense;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 签名生成工具
 */
public final class TgLicenseSignUtil {
    private static final Logger logger = LoggerFactory.getLogger(TgLicenseGenerator.class); // 日志

    private TgLicenseSignUtil() {
        throw new Error("工具类不能实例化！");
    }

    /**
     * 签名，防篡改
     */
    static String sign(final TgLicense licenseInfo) {
        String str = licenseInfo.getPackageName() // 包名
                + "_" + licenseInfo.getProduct() // 产品
                + "_" + licenseInfo.getLicenseType()  // 许可类型
                + "_" + licenseInfo.getIssuedDate(); // 生效日期
        if (isNotBlank(licenseInfo.getExpiryDate())) {
            str += "_" + licenseInfo.getExpiryDate(); // 过期日期
        }
        if (isNotBlank(licenseInfo.getVersionName())) {
            str += "_" + licenseInfo.getVersionName(); // 版本
        }
        if (isNotBlank(licenseInfo.getCustomer())) {
            str += "_" + licenseInfo.getCustomer(); // 客户
        }
        if (isNotBlank(licenseInfo.getCustomerType())) {
            str += "_" + licenseInfo.getCustomerType(); // 客户类型: 企业 ,个人
        }
        if (isNotBlank(licenseInfo.getCustomerCodeNum())) {
            str += "_" + licenseInfo.getCustomerCodeNum(); // 客户编码。企业的是企业信用代码，个人的是身份证号或手机号
        }
        if (isNotBlank(licenseInfo.getDeviceId())) {
            str += "_" + licenseInfo.getDeviceId(); // deviceId
        }
        if (isNotBlank(licenseInfo.getIpAddress())) {
            str += "_" + licenseInfo.getIpAddress(); // IP地址
        }
        if (isNotBlank(licenseInfo.getMacAddress())) {
            str += "_" + licenseInfo.getMacAddress(); // mac地址
        }
        return sha256Hex(str);
    }

    /**
     * 字符串非空
     */
    private static boolean isNotBlank(final String str) {
        return str != null && !str.isBlank();
    }

    /**
     * sha256Hex
     */
    private static String sha256Hex(final String str) {
        String result = null;
        if (isNotBlank(str)) {
            // 创建MessageDigest实例，指定使用的哈希算法
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                // 将要签名的数据转换为字节数组
                byte[] bytesToSign = str.getBytes(StandardCharsets.UTF_8);
                byte[] hashedBytes = digest.digest(bytesToSign);
                // 将字节数组转换为十六进制字符串
                StringBuilder hexString = new StringBuilder();
                for (byte b : hashedBytes) {
                    hexString.append(String.format("%02x", b));
                }
                result = hexString.toString();
            } catch (NoSuchAlgorithmException e) {
                logger.error("签名失败：" + e.getMessage());
            }
        }
        return result;
    }
}
