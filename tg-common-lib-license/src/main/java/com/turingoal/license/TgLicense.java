package com.turingoal.license;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * TgLicense
 */
public final class TgLicense implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private String licenseId = UUID.randomUUID().toString().trim().replaceAll("-", "");// licenseId保存在服务端
    private String packageName; // 包名 ***必填
    private String product; // 产品 ***必填
    private String licenseType; // 授权类型 ***必填
    private String issuedDate; // 生效日期 ***必填
    private String expiryDate; // 过期日期
    private String versionName; // 版本
    private String customer; // 客户 ***必填。企业的是企业名称，个人的是个人姓名
    private String customerType; // 客户类型 ***必填: 企业, 个人
    private String customerCodeNum; // 客户编码。企业的是企业信用代码，个人的是个人身份证号
    private String deviceId; // 设备id
    private String ipAddress; // IP地址
    private String macAddress; // mac地址
    // 其他信息
    private String licenseSign; // 签名


    public TgLicense() {
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerCodeNum() {
        return customerCodeNum;
    }

    public void setCustomerCodeNum(String customerCodeNum) {
        this.customerCodeNum = customerCodeNum;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getLicenseSign() {
        return licenseSign;
    }

    public void setLicenseSign(String licenseSign) {
        this.licenseSign = licenseSign;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TgLicense tgLicense = (TgLicense) o;
        return Objects.equals(licenseId, tgLicense.licenseId) && Objects.equals(packageName, tgLicense.packageName) && Objects.equals(product, tgLicense.product) && Objects.equals(licenseType, tgLicense.licenseType) && Objects.equals(issuedDate, tgLicense.issuedDate) && Objects.equals(expiryDate, tgLicense.expiryDate) && Objects.equals(versionName, tgLicense.versionName) && Objects.equals(customer, tgLicense.customer) && Objects.equals(customerType, tgLicense.customerType) && Objects.equals(customerCodeNum, tgLicense.customerCodeNum) && Objects.equals(deviceId, tgLicense.deviceId) && Objects.equals(ipAddress, tgLicense.ipAddress) && Objects.equals(macAddress, tgLicense.macAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(licenseId, packageName, product, licenseType, issuedDate, expiryDate, versionName, customer, customerType, customerCodeNum, deviceId, ipAddress, macAddress);
    }

    @Override
    public String toString() {
        return "TgLicense{" +
                "licenseId='" + licenseId + '\'' +
                ", packageName='" + packageName + '\'' +
                ", product='" + product + '\'' +
                ", licenseType='" + licenseType + '\'' +
                ", issuedDate='" + issuedDate + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", versionName='" + versionName + '\'' +
                ", customer='" + customer + '\'' +
                ", customerType='" + customerType + '\'' +
                ", customerCodeNum='" + customerCodeNum + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", macAddress='" + macAddress + '\'' +
                ", licenseSign='" + licenseSign + '\'' +
                '}';
    }
}
