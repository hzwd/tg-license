package com.turingoal.license.checker;

/**
 * 常量-》客户类型
 */
public interface TgConstantLicenseCustomerTypes {
    String CUSTOMER_TYPE_COMPANY = "企业";
    String CUSTOMER_TYPE_INDIVIDUAL = "个人";
}
