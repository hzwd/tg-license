package com.turingoal.license.checker;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/**
 * 反序列化
 */
public class TgObjectInputStream extends ObjectInputStream {
    private static final String OLD_PACKAGE = "com.turingoal.license";
    private static final String NEW_PACKAGE = "com.turingoal.license.checker";

    public TgObjectInputStream(InputStream arg0) throws IOException {
        super(arg0);
    }

    @Override
    public Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        String name = desc.getName();
        try {
            if (name.startsWith(OLD_PACKAGE)) {
                name = name.replace(OLD_PACKAGE, NEW_PACKAGE);
                return Class.forName(name);
            }
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return super.resolveClass(desc);
    }
}