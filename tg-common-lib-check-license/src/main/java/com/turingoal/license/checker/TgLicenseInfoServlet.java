package com.turingoal.license.checker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;

/**
 * 获取license信息
 */
public class TgLicenseInfoServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        TgLicense licenseInfo = TgLicenseChecker.getLicenseInfo();
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>licenseInfo</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h2>====== license信息 ======</h2>");
            if (!licenseInfo.isExist()) {
                out.println("<h3>license文件不存在 </h3>");
            }
            if (!licenseInfo.isOk()) {
                out.println("<h3>license文件不合法 </h3>");
            }
            if (licenseInfo.isChanged()) {
                out.println("<h3>license文件被非法修改 </h3>");
            }
            out.println("<h4>licenseId：" + licenseInfo.getLicenseId() + "</h4>");
            out.println("<h4>产品名称：" + licenseInfo.getProduct() + "</h4>");
            out.println("<h4>授权类型：" + licenseInfo.getLicenseType() + "</h4>");
            if (TgLicenseUtil.isNotBlank(licenseInfo.getVersionName())) {
                out.println("<h4>版本名称：" + licenseInfo.getVersionName() + "</h4>");
            }
            out.println("<h4>生效时间：" + licenseInfo.getIssuedDate() + "</h4>");
            if (licenseInfo.getExpiryDate() != null) {
                out.println("<h4>到期时间：" + licenseInfo.getExpiryDate());
                if (licenseInfo.isExpired()) {
                    out.println(" 【过期】 </h4>");

                } else {
                    out.println(" 【正常】 </h4>");
                }
            }
            if (TgLicenseUtil.isNotBlank(licenseInfo.getCustomer())) {
                out.println("<h4>客户名称：" + licenseInfo.getCustomer() + "</h4>");
            }
            if (TgLicenseUtil.isNotBlank(licenseInfo.getCustomerType())) {
                out.println("<h4>客户类型：" + licenseInfo.getCustomerType() + "</h4>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public String getServletInfo() {
        return "LicenseInfoServlet";
    }
}
