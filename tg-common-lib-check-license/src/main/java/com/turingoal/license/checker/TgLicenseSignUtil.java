package com.turingoal.license.checker;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 序列号生成工具
 */
public final class TgLicenseSignUtil {
    private TgLicenseSignUtil() {
        throw new Error("工具类不能实例化！");
    }

    /**
     * 签名，防篡改
     */
    static String sign(final TgLicense licenseInfo) {
        String str = licenseInfo.getPackageName() // 包名
                + "_" + licenseInfo.getProduct() // 产品
                + "_" + licenseInfo.getLicenseType()  // 许可类型
                + "_" + licenseInfo.getIssuedDate(); // 生效日期
        if (licenseInfo.getExpiryDate() != null) {
            str += "_" + licenseInfo.getExpiryDate(); // 过期日期
        }
        if (TgLicenseUtil.isNotBlank(licenseInfo.getVersionName())) {
            str += "_" + licenseInfo.getVersionName(); // 版本
        }
        if (TgLicenseUtil.isNotBlank(licenseInfo.getCustomer())) {
            str += "_" + licenseInfo.getCustomer(); // 客户
        }
        if (TgLicenseUtil.isNotBlank(licenseInfo.getCustomerType())) {
            str += "_" + licenseInfo.getCustomerType(); // 客户类型: 企业 ,个人
        }
        if (TgLicenseUtil.isNotBlank(licenseInfo.getCustomerCodeNum())) {
            str += "_" + licenseInfo.getCustomerCodeNum(); // 客户编码。企业的是企业信用代码，个人的是身份证号或手机号
        }
        if (TgLicenseUtil.isNotBlank(licenseInfo.getDeviceId())) {
            str += "_" + licenseInfo.getDeviceId(); // DeviceId
        }
        if (TgLicenseUtil.isNotBlank(licenseInfo.getIpAddress())) {
            str += "_" + licenseInfo.getIpAddress(); // IP地址
        }
        if (TgLicenseUtil.isNotBlank(licenseInfo.getMacAddress())) {
            str += "_" + licenseInfo.getMacAddress(); // mac地址
        }
        return sha256Hex(str);
    }

    /**
     * sha256Hex
     */
    private static String sha256Hex(final String str) {
        String result = null;
        if (TgLicenseUtil.isNotBlank(str)) {
            // 创建MessageDigest实例，指定使用的哈希算法
            try {
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                // 将要签名的数据转换为字节数组
                byte[] bytesToSign = str.getBytes(StandardCharsets.UTF_8);
                byte[] hashedBytes = digest.digest(bytesToSign);
                // 将字节数组转换为十六进制字符串
                StringBuilder hexString = new StringBuilder();
                for (byte b : hashedBytes) {
                    hexString.append(String.format("%02x", b));
                }
                result = hexString.toString();
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
        return result;
    }
}
