package com.turingoal.license.checker;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * LicenseInfo
 */
public final class TgLicense implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private String licenseId; // licenseId
    private String packageName; // 包名
    private String product; // 产品
    private String licenseType; // 授权类型
    private String issuedDate; // 生效日期
    private String expiryDate; // 过期日期
    private String versionName; // 版本
    private String customer; // 客户。企业的是企业名称，个人的是个人姓名
    private String customerType; // 客户类型: 企业, 个人
    private String customerCodeNum; // 客户编码。企业的是企业信用代码，个人的是个人身份证号
    private String deviceId; // 设备id
    private String ipAddress; // IP地址
    private String macAddress; // mac地址
    // 其他信息
    private String licenseSign; // 签名
    private String licenseInfoUrl;  // license信息url
    private boolean exist; // 是否存在
    private boolean ok; // 合法
    private boolean expired; // 是否过期
    private boolean changed; // 是否被篡改


    public TgLicense() {
    }

    public String getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(String licenseId) {
        this.licenseId = licenseId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerCodeNum() {
        return customerCodeNum;
    }

    public void setCustomerCodeNum(String customerCodeNum) {
        this.customerCodeNum = customerCodeNum;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getLicenseSign() {
        return licenseSign;
    }

    public void setLicenseSign(String licenseSign) {
        this.licenseSign = licenseSign;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public String getLicenseInfoUrl() {
        return licenseInfoUrl;
    }

    public void setLicenseInfoUrl(String licenseInfoUrl) {
        this.licenseInfoUrl = licenseInfoUrl;
    }

    /**
     * getExistStr
     */
    public String getExistStr() {
        if (exist) {
            return "是";
        } else {
            return "否";
        }
    }

    /**
     * getOkStr
     */
    public String getOkStr() {
        if (ok) {
            return "是";
        } else {
            return "否";
        }
    }

    /**
     * getExpiredStr
     */
    public String getExpiredStr() {
        if (expired) {
            return "是";
        } else {
            return "否";
        }
    }

    /**
     * getChangedStr
     */
    public String getChangedStr() {
        if (changed) {
            return "是";
        } else {
            return "否";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TgLicense license = (TgLicense) o;
        return Objects.equals(licenseId, license.licenseId) && Objects.equals(packageName, license.packageName) && Objects.equals(product, license.product) && Objects.equals(licenseType, license.licenseType) && Objects.equals(issuedDate, license.issuedDate) && Objects.equals(expiryDate, license.expiryDate) && Objects.equals(versionName, license.versionName) && Objects.equals(customer, license.customer) && Objects.equals(customerType, license.customerType) && Objects.equals(customerCodeNum, license.customerCodeNum) && Objects.equals(deviceId, license.deviceId) && Objects.equals(ipAddress, license.ipAddress) && Objects.equals(macAddress, license.macAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(licenseId, packageName, product, licenseType, issuedDate, expiryDate, versionName, customer, customerType, customerCodeNum, deviceId, ipAddress, macAddress);
    }

    @Override
    public String toString() {
        return "TgLicense{" +
                "licenseId='" + licenseId + '\'' +
                ", packageName='" + packageName + '\'' +
                ", product='" + product + '\'' +
                ", licenseType='" + licenseType + '\'' +
                ", issuedDate='" + issuedDate + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", versionName='" + versionName + '\'' +
                ", customer='" + customer + '\'' +
                ", customerType='" + customerType + '\'' +
                ", customerCodeNum='" + customerCodeNum + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", macAddress='" + macAddress + '\'' +
                ", licenseSign='" + licenseSign + '\'' +
                ", licenseInfoUrl='" + licenseInfoUrl + '\'' +
                ", exist=" + exist +
                ", ok=" + ok +
                ", expired=" + expired +
                ", changed=" + changed +
                '}';
    }
}
