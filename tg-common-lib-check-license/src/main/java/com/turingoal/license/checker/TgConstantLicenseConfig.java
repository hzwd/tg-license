package com.turingoal.license.checker;

/**
 * 常量-》配置
 */
public interface TgConstantLicenseConfig {
    String LICENSE_FILE_PATH = "license.lic";
    String SERVER_DOMAIN = ""; // license检查server
    String LICENSE_CHECK_URL = ""; // license检查url
    String LICENSE_INFO_URL = ""; // license信息url
}
