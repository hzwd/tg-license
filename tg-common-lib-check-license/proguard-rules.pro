#############################################
# 对于一些基本指令的添加
#############################################
#代码混淆压缩比，在0~7之间，默认为5，一般不做修改
#-optimizationpasses 5
# 使用printmapping指定映射文件的名称
#-printmapping priguardMapping.txt
#-printusage unused.txt
# 避免混淆异常
-keepattributes Exceptions
#-ignorewarnings
#############################################
# Android开发中一些需要保留的公共部分
#############################################
#保留我们使用的四大组件，自定义的Application等等这些类不被混淆,因为这些子类都有可能被外部调用
#保留support下的所有类及其内部类
#保留R下面的资源
# 对R文件下的所有类及其方法，都不能被混淆
#保留本地native方法不被混淆
#保留在Activity中的方法参数是view的方法，这样以来我们在layout中写的onClick就不会被影响
#保留枚举类不被混淆
#保留我们自定义控件（继承自View）不被混淆
#保留Parcelable序列化类不被混淆
#保留Serializable序列化的类不被混淆
#对于带有回调函数的onXXEvent的，不能被混淆
#webView处理，项目中没有使用到webView忽略即可
#############################################
# 第三方依赖库
#############################################
### arouter ###
### ButterKnife ###
### EventBus ###
### glide ###
# for DexGuard only
# -keepresourcexmlelements manifest/application/meta-data@value=GlideModule
### greenDAO 3 ###
### gson ###
###  极光推送 ###
### lombok ###
-keep class lombok.** { *; }
-dontwarn lombok.**
### modbus4j ###
-dontwarn com.serotonin.modbus4j.**
### okhttp ###
### okio ###
### pdfViewer ###
-keep class  com.github.barteksc.**{*;}
### picture select  ###
-keep class com.luck.picture.lib.** {*;}
###  Retrolambda ###
-dontwarn java.lang.invoke.*
### retrofit2 ###
### rxjava ###
### rxandroid ###
###  ucrop 裁剪 ###
-dontwarn com.yalantis.ucrop**
-keep class com.yalantis.ucrop** { *; }
-keep interface com.yalantis.ucrop** { *; }
### utilcode 工具包 ###
-keep class com.blankj.utilcode.** { *; }
-keepclassmembers class com.blankj.utilcode.** { *; }
-dontwarn com.blankj.utilcode.**
### zip4j ###
-dontwarn net.lingala.zip4j.**
###  支付宝 ###
#############################################
# 当前app
#############################################
# dontwarn 不提示警告
# keep 保持原样不混淆
-dontwarn com.turingoal.bts.common.**
-keep class com.turingoal.bts.common.**{*;}
-keep class com.turingoal.bts.pda.follow.bean.** { *; }
-keep class com.turingoal.bts.pda.follow.app.** { *; }

# camerakit 拍照
-dontwarn com.google.android.gms.**
-keepclasseswithmembers class com.wonderkiln.camerakit.SurfaceViewPreview {
    native <methods>;
}

# qmui
-keep class **_FragmentFinder { *; }
-keep class com.qmuiteam.qmui.arch.record.** { *; }
-keep class androidx.fragment.app.* { *; } # if use androidx
#-keep class android.support.v4.app.* { *; } # if use support v4