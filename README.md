# tg-license    （糖果）license工具包

#### 介绍
简单实现license的生成与验证

包括两个部分：

tg-common-lib-license 用来生成license  

tg-common-lib-check-license  用来校验license  


#### 实现逻辑
生成：license基本信息-> 签名-> license对象序列化-> 私钥加密-> 文件头+公钥+序列化后的数据-> 写到文件 

校验：读取文件-> 校验并去掉文件头-> 取得公钥-> 用公钥解密-> 反序列化-> 获取license基本信息和签名-> 重新签名对比是否篡改-> 校验其他信息（产品、授权类型、过期日期、deviceId、IP、mac地址等）

#### license管理系统：生成
1、可以开发一个license管理系统，将tg-common-lib-license引入  
   
2、创建一个对象 ： com.turingoal.license.TgLicense   

| 属性  | 名称  |  是否必填 | 备注  |
|---|---|---|---|
|  licenseId  | id  |  是 |  uuid格式，自动生成 |
|  packageName |  包名 | 是 |  用来区分产品 |
|  product |  产品名称 |  是  | 产品名称  |
|  licenseType | 授权类型 | 是 | TgConstantLicenseTypes常量：开发版（每天会自动退出运行），免费版（暂时没做限制），专业版 |
|  issuedDate |  生效日期 | 是 | 生效日期，没做限制 |
|  expiryDate |  过期日期 | 否 | 不填的话不做限制，到期系统会无法启动，到期30天前会打印日志提醒 |
|  versionName|  版本号 | 否 | 暂时没做限制 |
|  customer| 客户 | 是 | 企业的是企业名称，个人的是个人姓名 |
|  customerType| 客户类型 | 是 | TgConstantLicenseCustomerTypes常量：企业, 个人 |
|  customerCodeNum| 客户编码 | 否 | 企业的是企业信用代码，个人的是个人身份证号 |
|  deviceId| 设备deviceId | 否 | 设备id，不填的话不限制，填了的话会判断是否和当前设备deviceId一致，不一致自动退出。com.github.oshi:oshi-core  OshiUtil.getSystem().getSerialNumber()获取 |
|  ipAddress| 设备ip地址 | 否 | 设备ip，不填的话不限制，填了的话会判断是否和当前设备ip一致 |
|  macAddress| 设备mac地址 | 否 | 设备mac，不填的话不限制，填了的话会判断是否和当前设备mac一致 |

3、生成公钥和秘钥：TgLicenseEncryptUtil.createKeyPair,  getPrivateKeyStr,  getPublicKeyStr 。 私钥自己保存后面可以重新生成license

4、生成license文件 ：TgLicenseGenerator.createLicenseAndSaveFile 

5、可以写一个根据licenseId获取license信息的接口 ：licenseInfoUrl ，校验端可以通过这个接口查询license信息

6、可以写一个接收校验license信息的接口 ：licenseCheckUrl，校验端每次本地校验后会发送信息到这个接口，方便统计

#### 项目：校验  

1.  tg-common-lib-check-license 引入到项目中  

2.  初始化
    TgLicenseChecker.init(TgBootApplication.class, port, contextPath, licenseServerDomain, licenseCheckUrl, licenseInfoUrl); // 校验license
    TgLicenseChecker.start(); // 只能执行一次，每隔一段时间会自动校验一次

| 参数  | 名称  |  是否必填 | 备注 |
|---|---|---|---|
|  Class |  基础包下的类 |  否 | 项目基础包名下的随便一个类，主要是用来获取包名  |
|  port |  端口 |  否 |  当前项目端口，会发送到校验服务接口 |
|  contextPath | 项目contextPath  |  否 |  当前项目contextPath，会发送到校验服务接口 |
|  licenseServerDomain | 服务器ip或域名  |  否  |  查询和校验的服务器ip或域名。TgConstantLicenseConfig里可以设置一个默认的|
|  licenseCheckUrl |  接收校验接口地址 |  否  |  接收校验license信息的接口。TgConstantLicenseConfig里可以设置一个默认 |
|  licenseInfoUrl |  查询接口地址 |  否  |  根据licenseId获取license信息的接口。TgConstantLicenseConfig里可以设置一个默认 |

3、手动调用TgLicenseChecker.checkAll

4、手动查询license信息 TgLicenseChecker.getLicenseInfo 

5、注册Servlet:TgLicenseInfoServlet或者TgLicenseInfoJakartaServlet（JDK17及以上）以便远程查询当前项目license信息

#### 写到最后
没啥写的了 